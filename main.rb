require_relative 'lib/two_sum'
require_relative 'lib/median_calc'

file = '100000.txt'
file = 'test.txt'
file = 'algo.txt'

file2 = 'test10000.txt'
file2 = 'Median.txt'

comp = TwoSum.new(File.open(file, 'r'))
comp.compute!
puts comp.sums

medians = MedianCalc.new(File.open(file2))

medians.compute!

puts medians.sum_of_medians