require 'set'
require 'bigdecimal'

TwoSum = Struct.new(:file) do
  def compute!
    parse

    # puts @data.inspect
    @seen_answers = Set.new

    hash_stuff
    # seq_stuff
  end

  def hash_stuff
    @data.each do |group, values|
      values.each do |x|
        [-group-2, -group-1, -group].uniq.flat_map do |key|
          @data[key] || []
        end.each do |y|
          @seen_answers.add(x+y) if x !=y && (x+y).abs <= 10_000
        end
      end
    end
  end

  def sums
    # puts @seen_answers.inspect
    @seen_answers.size
  end

  private

  def parse
    @data = {}
    file.each do |line|
      @data[line.to_i/10000] ||= []
      @data[line.to_i/10000].push line.to_i
    end
  end
end