require 'algorithms'

MedianCalc = Struct.new(:file) do
  def compute!
    @min = Containers::MaxHeap.new
    @max = Containers::MinHeap.new

    @medians = []

    file.each do |line|
      add(line.to_i)

      @medians.push median
    end
  end

  def sum_of_medians
    @medians.inject(:+) % 10_000
  end

  private

  def add(value)
    if value <= median.to_i
      @max.push @min.next! if @min.size-@max.size == 1
      @min.push value
    else
      @min.push @max.next! if @max.size-@min.size == 1
      @max.push value
    end
  end

  def median
    if @min.size == @max.size
      @min.next
    elsif @min.size > @max.size
      @min.next
    else
      @max.next
    end
  end

  def total_size
    @min.size + @max.size
  end
end